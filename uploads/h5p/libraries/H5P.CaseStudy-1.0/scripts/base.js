/**
 * Created by sysadmin on 28/8/14.
 */
var Base = new function Base(){
    var instance = this;
    var slideInitCallBack = undefined;
    var n_event = undefined;
    var p_event = undefined;

    Base.getInstance = function(){
        return instance;
    };
    Base.staticMethod = function(){
        alert( "static method called!" );
    };
    Base.setSlideInitCallBack = function(call_back){
        slideInitCallBack = call_back;
    };
    Base.initSlideCallBack = function(){
        if(typeof slideInitCallBack != 'undefined'){
            slideInitCallBack();
        }
    };
    Base.setNextEventElement = function(next_event){
        n_event = next_event;
    };
    Base.getNextEventElement = function(){
        return n_event;
    };
    Base.setPreviousEventElement = function(previous_event){
        p_event = previous_event;
    };
    Base.getPreviousEventElement = function(){
        return p_event;
    };
    this.resetCallBack = function(){
        slideInitCallBack = undefined;
    };
    return Base;
};

Base.prototype.toString = function(){
    return "[object Singleton]";
};

Base.prototype.isSlideButtonActive = function(){
    return "[object Singleton]";
};

Base.prototype.addInActiveClass = function($ele){
    $ele.addClass("in-active");
};

Base.prototype.removeInActiveClass = function($ele){
    $ele.removeClass("in-active");
};

Base.prototype.hasInActiveClass = function($ele){
    return $ele.hasClass("in-active") ? true : false;
};

Base.prototype.parseHTML = function(str){
    var regex = /(https?:\/\/([-\w\.]+)+(:\d+)?(\/([\w\/_\.]*(\?\S+)?)?)?)/ig;
    return str;
};