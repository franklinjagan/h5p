/**
 * Created by sysadmin on 16/8/14.
 */
var H5P = H5P || {};

/**
 *
 * @constructor
 */
H5P.Introduction = function(contents,content_id){
    this.contents = contents;
    this.contentId = content_id;
};
H5P.Introduction.prototype.attach = function(container){
    var paragraph = "<div class=\"case-study-body\">"+
        "<div class=\"case-study\">"+
        "<h1>"+this.contents.title+"</h1>"+
        "<h2>"+this.contents.subTitle+"</h2>"+
        "<p>In this lesson, you will:</p>"+
        "<ul>"+
        "<li>Apply your knowledge of expert communication,procedural and informed decision-making strategies in a Family Planning/Contraceptive choice consultation</li>"+
        "<li>Evaluates case data for a client in need of non-emergency contraception</li>"+
        "<li>Recommend appropriate tests and/or examination If needed to properly assess medical eligibility for non-emergency contraception options</li>"+
        "<li>Review how to execute appropriate family planning consultation related clinical skills as applicable</li>"+
        "</ul>"+
        "<p class=\"paragraph\">The required readings and supplemental resources that will be helpful as you work through this self-directed lesson are available via the Resource link at the upper right of your screen</p>"+
        "<p class=\"paragraph\">Right now, take time to famillaries yourself with the available resource.</p>"+
        "<p class=\"paragraph\">We will cover these resource more thoroughly as this lesson progresses, but feel free to visit these resource at any time.</p>"+
        "</div>" +
        "<div class=\"resource\">"+
        "<div>"+
        "<p class=\"paragraph\">For this case study,the setting is an urban NGO clinic in Africa where you have been practicing for three years. You work with five other doctors and a staff of four.</p>"+
        "<p class=\"paragraph\">You will be working together with you clients to help them with making decisions about their contraceptive choices.</p>"+
        "<p class=\"paragraph\">Your first client today is <span>Kijai</span>.Begin by reading her comment, then click on her medical chart to read more about her.You may revisit this chart at any point in the conversation by clicking the chart icon.</p>"+
        "<p class=\"paragraph\">Using this information, choose the best response to her comment.</p>"+
        "</div>"+
        "<div class=\"image-frame\">"+
        "<div class=\"image\">"+
        "</div>"+
        "</div>"+
        "</div>"+
        "</div>";
    container.html(paragraph);
};